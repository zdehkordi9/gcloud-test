const express = require('express');
const app = express();

const mysql = require('./mysql.js')

app.get('/', async (req, res) => {

    const pool = await mysql();

    const data = await pool.query(
        'SELECT candidate, time_cast FROM votes ORDER BY time_cast DESC LIMIT 5'
    );

    res.send(data);
});

const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`helloworld: listening on port ${port}`);
});